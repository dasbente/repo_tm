import numpy as np
import math


def as_impulse_vector(df, resolution=1000):
    end_times = df["END"]
    last = end_times.iloc[-1]

    v = np.zeros(math.ceil(last / resolution)) # impulse vector

    succs = df["SUCCESS"]
    scores = np.floor(end_times[succs] / resolution).astype(int)
    v[scores] = 1
    return v.astype(float)


# gaussian blur
def gaussian(x, mu, sig):
    return np.exp(-np.power(x - mu, 2.) / (2 * np.power(sig, 2.)))


# gaussian convolution filter (1D)
def gaussian_filter(w, sigma):
    filter = np.arange(-w, w, 1.0)
    return gaussian(filter, 0, sigma)


def staircase(x):
    res = np.zeros(x.shape[0])
    res[0] = x[0]
    for i in range(1, x.shape[0]):
        res[i] = res[i - 1] + x[i]

    return res
