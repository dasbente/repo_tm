import pandas
import math

import matplotlib.pyplot as plt
import numpy as np


# import relevant data as pandas data frames for later use
def import_data():
    events = pandas.read_csv("data/Session_Data.csv")
    participants = pandas.read_csv("data/Participants.csv")
    matching_data = pandas.read_csv("data/Group_Matching.csv")

    return {
        # data associated with each (non-movement) input event
        "events": events,

        # data associated with each specific participant
        "participants": participants,

        # associations of session id's with user groups
        "groups": matching_data,

        # merges above collections so that each user is associated with each input event
        "merged": participants.merge(matching_data, on="GROUP").merge(events, on="SESSION_ID")
    }


# generates a set of simple histograms for a data frame and a given list of keys
def hist_keys(dataframe, keys, suptitle="", rows=1, figsize=(20, 3), mean=False):
    fig, a = plt.subplots(rows, math.floor(len(keys) / rows), figsize=figsize)
    fig.suptitle(suptitle)

    for i in range(0, len(keys)):
        k = keys[i]
        data = dataframe[k]
        label = k

        if mean:
            mean = data.mean()
            a[i].axvline(mean, color="k", linewidth=1)
            label = "{0}, mean: {1:.2f}".format(label, mean)

        a[i].set(xlabel=label)
        a[i].hist(data)
    return fig, a


# generates a grid of scatter plots from two sets of keys and a data frame
def scatter_grid(dataframe, x_keys, y_keys, suptitle="", subfig_size=(2, 2)):
    rows, cols = (len(y_keys), len(x_keys))
    fig, a = plt.subplots(rows, cols, figsize=(rows * subfig_size[0], cols * subfig_size[1]))

    for i in range(0, rows):
        for j in range(0, cols):
            a[i, j].set(xlabel=x_keys[j])
            a[i, j].set(ylabel=y_keys[i])
            a[i, j].scatter(dataframe[x_keys[j]], dataframe[y_keys[i]])

    return fig, a


def count_unique(df, index):
    return df.pivot_table(index=index, aggfunc="size")
