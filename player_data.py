from main import import_data, count_unique
import pandas as pd


# groups player data by event into a easier to use map
def calc_player_data():
    player_data = {}
    events = import_data()["events"]
    events = events[events["START"] > 1] # remove broken time stamped events

    player_indices = events[["SESSION_ID", "PLAYER"]].set_index(["SESSION_ID", "PLAYER"]).index.unique()

    session_start_times = {}
    for id in events["SESSION_ID"].unique():
        session_start_times[id] = events[events["SESSION_ID"] == id]["START"].min()

    for index in player_indices:
        sess_id, player = index
        query = (events["PLAYER"] == player) & (events["SESSION_ID"] == sess_id)
        player_data[index] = events[query].sort_values(by="START")
        player_data[index]["START"] = player_data[index]["START"] - session_start_times[sess_id]
        player_data[index]["END"] = player_data[index]["END"].astype(int) - session_start_times[sess_id]

    return player_data, player_indices
