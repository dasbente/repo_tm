#!/usr/bin/python3.8
from player_data import calc_player_data
import json
import numpy as np
from filters import as_impulse_vector

players, p_inds = calc_player_data()

p_list = []
for i in p_inds:
    player = players[i]

    timestamps = (player["START"] / 1000.0).to_list()

    # preprocess score metrics
    success = player["SUCCESS"]
    succ_q = success == True
    fail_q = success == False

    succs = np.zeros(success.shape)
    succs[succ_q] = 1

    simple_score = succs.copy()
    simple_score[fail_q] = -1

    game_score = 2 * succs
    game_score[fail_q] = -1

    v = as_impulse_vector(player)
    rolling_sum = np.convolve(v, np.ones(30), mode="same")

    p_list.append({
        "player": i[1],
        "session": i[0],
        "timestamps": timestamps,
        "simpleScore": simple_score.tolist(),
        "rollingSum": rolling_sum.tolist()
    })

print(json.dumps(p_list))