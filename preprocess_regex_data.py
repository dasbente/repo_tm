#!/usr/bin/python3.8
import json
from player_data import calc_player_data
from regex_data import regex_stats

pdata, inds = calc_player_data()

reg_stats = []

for i in inds:
    stats = regex_stats(pdata[i])

    reg_stats.append({
        "player": i[1],
        "session": i[0],
        "stats": stats[['REGEX', "succ_ratio"]].values.tolist()
    })

print(json.dumps(reg_stats))