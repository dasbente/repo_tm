from main import import_data, count_unique
import pandas as pd


# calculates some statistics for the regular expressions in the events data frame
def regex_stats(data = None):
    if data is None:
        data = import_data()["events"]

    expr = count_unique(data, ["REGEX"])
    expr.name = "sum"

    succs = count_unique(data[data["SUCCESS"]], ["REGEX"])
    succs.name = "succ"

    fails = expr - succs
    fails.name = "fail"

    succ_ratio = succs / expr
    succ_ratio.name = "succ_ratio"

    res = pd.concat([expr, succs, fails, succ_ratio], axis=1).sort_values(by="succ_ratio")

    return res.reset_index()


# same as above, but considers words instead of regular expressions
def word_stats():
    data = import_data()["events"]

    expr = count_unique(data, ["WORD"])
    expr.name = "sum"

    succs = count_unique(data[data["SUCCESS"]], ["WORD"])
    succs.name = "succ"

    fails = expr - succs
    fails.name = "fail"

    succ_ratio = succs / expr
    succ_ratio.name = "succ_ratio"

    return pd.concat([expr, succs, fails, succ_ratio], axis=1).sort_values(by=["succ_ratio"])