import React from 'react';
import './App.css';
import SessionChoice from './features/sessionChoice/SessionChoice'
import SessionScores from './features/players/SessionScores'
import PlayerList from './features/playerChoice/PlayerList'
import PlayerRollingSum from './features/playerChoice/PlayerRollingSum'
import RegexHeatmap from './features/regex/RegexHeatmap'

const App = () => {
  return (
    <div className="container-fluid">
      <div className="row mt-4 mb-2">
        <div className="col-3">
          <SessionChoice />
        </div>
        <div className="col-9">
          <SessionScores />
        </div>
      </div>
      <div className="row">
        <div className="col-3">
          <PlayerList />
        </div>
        <div className="col-9">
          <PlayerRollingSum />
        </div>
      </div>
      <div className="row">
        <RegexHeatmap />
      </div>
    </div>
  )
}

export default App;
