import { configureStore } from '@reduxjs/toolkit'
import playerReducer from '../features/players/playerSlice'
import sessionReducer from '../features/sessionChoice/sessionSlice'
import styleSlice from '../features/style/styleSlice'
import playerChoiceReducer from '../features/playerChoice/playerChoiceSlice'
import regexSlice from '../features/regex/regexSlice'

export default configureStore({
  reducer: {
    players: playerReducer,
    session: sessionReducer,
    style: styleSlice,
    player: playerChoiceReducer,
    regex: regexSlice
  },
});
