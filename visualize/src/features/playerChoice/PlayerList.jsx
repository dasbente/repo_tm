import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { selectPlayers, selection, select } from './playerChoiceSlice'

const PlayerItem = ({player, i}) => {
  const dispatch = useDispatch()
  const selected = useSelector(selection)

  return (
    <button
      className={`list-group-item ${selected === i ? 'active' : ''}`}
      onClick={() => dispatch(select(i))}
    >
      {player.player}
    </button>
  )
}

const PlayerList = () => {
  const players = useSelector(selectPlayers)

  return (
    <ul className="list-group">
      {players.map((p, i) => <PlayerItem player={p} i={i} />)}
    </ul>
  )
}

export default PlayerList
