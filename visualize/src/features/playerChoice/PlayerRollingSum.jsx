import React from 'react'
import Chart from 'react-apexcharts'
import { useSelector } from 'react-redux'
import { selectedPlayer } from './playerChoiceSlice'
import { curveStyleSelector } from '../style/styleSlice'
import { selectedSessionId } from '../sessionChoice/sessionSlice'

const PlayerRollingSum = () => {
  const player = useSelector(selectedPlayer)
  const session = useSelector(selectedSessionId)
  const curveStyle = useSelector(curveStyleSelector)

  const options = {
    chart: { id: 'rolling-sum', type: 'area' },
    title: { text: 'Rolling Sum over Score of ' + player.player + " in " + session },
    stroke: { curve: curveStyle, show: false },
    xaxis: { type: 'numeric' },
    fill: { type: 'solid', opacity: 0.5 }
  }

  const series = [{
    data: player.rollingSum,
    type: 'area'
  }]

  return (
    <div>
      <Chart options={options} series={series} height={500} width={1000} />
    </div>
  )
}

export default PlayerRollingSum
