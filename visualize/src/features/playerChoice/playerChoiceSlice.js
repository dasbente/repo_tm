import { createSlice } from '@reduxjs/toolkit'
import { selectedSessionsData } from '../sessionChoice/sessionSlice'

export const playerChoiceSlice = createSlice({
  name: 'player',
  initialState: 0,
  reducers: {
    select: (state, action) => action.payload
  }
})

export const { select } = playerChoiceSlice.actions

export const selection = ({ player }) => player
export const selectPlayers = state => selectedSessionsData(state)
export const selectedPlayer = state => selectedSessionsData(state)[selection(state)]

export default playerChoiceSlice.reducer
