import React from 'react'
import Chart from 'react-apexcharts'
import { useSelector } from 'react-redux'
import { selectedSessionsData, selectedSessionId } from '../sessionChoice/sessionSlice'
import CurveStyle from '../style/CurveStyle'
import Weight from '../style/Weight'
import { curveStyleSelector, weightSelector } from '../style/styleSlice'

const toSeries = ({ timestamps, simpleScore, player }, successWeight = 1, errorWeight = 0) => {
  const data = [...timestamps]

  let compoundScore = 0
  simpleScore.forEach((score, i) => {
    compoundScore += score * (score > 0 ? successWeight : -errorWeight)
    data[i] = { x: data[i], y: compoundScore }
  });

  return { type: 'line', name: player, data }
}

const SessionScores = () => {
  const players = useSelector(selectedSessionsData)

  const curveStyle = useSelector(curveStyleSelector)
  const successWeight = useSelector(weightSelector('success'))
  const failWeight = useSelector(weightSelector('fail'))
  const session = useSelector(selectedSessionId)

  const options = {
    chart: {
      id: 'session-scores',
    },
    title: { text: 'Score Progression in Session ' + session },
    stroke: { curve: curveStyle },
    xaxis: { type: 'numeric' }
  }

  const series = players.map(p => toSeries(p, successWeight, failWeight))

  return (
    <div>
      <Chart options={options} series={series} height="500" width="1000"/>
      <div className="row">
        <div className="col">
          <CurveStyle />
        </div>
        <div className="col">
          <Weight type="success" />
        </div>
        <div className="col">
          <Weight type="fail" />
        </div>
      </div>
    </div>
  )
}

export default SessionScores
