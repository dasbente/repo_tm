import { createSlice } from '@reduxjs/toolkit'
import data from '../../data/player_data.json'

export const dataSlice = createSlice({
  name: 'players',
  initialState: data,
  reducers: {}
})

export const selectSessions = ({ players }) => {
  const sessions = {}

  players.foreach(p => {
    if (!sessions[p.session]) sessions[p.session] = {}
    sessions[p.session][p.player] = p
  })

  return sessions
}

export const selectPlayerIds = ({ players }) => players.map(
  ({ player, session }) => ({ player, session })
)

export const selectPlayer = (sessId, playerName) => ({ players }) => players.find(
  ({ session, player }) => session === sessId && player === playerName
)

export default dataSlice.reducer
