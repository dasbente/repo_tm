import React from 'react'
import Chart from 'react-apexcharts'
import { selectData, regexLabels } from './regexSlice'
import { useSelector } from 'react-redux'

const RegexHeatmap = () => {
  const labels = useSelector(regexLabels)
  const label2X = labels.reduce((res, l, i) => { res[l] = i; return res }, {})

  const toSeries = ({ player, session, stats }) => {
    const data = labels.map(l => ({ x: l, y: -1 }))
    stats.forEach(([x, y]) => data[label2X[x]] = { x, y })
    return { name: `${session}, ${player}`, data }
  }

  const data = useSelector(selectData)
  const options = {
    chart: { type: 'heatmap' },
    xaxis: { categories: labels },
    plotOptions: {
      heatmap: {
        shadeIntensity: 0.5,
        radius: 0,
        useFillColorAsStroke: true,
        colorScale: {
          ranges: [
            { from: -1, to: -1, name: ' ', color: '#ffffff' },
            { from: 0, to: 0.7, name: 'Failed', color: '#aa0000' },
            { from: 0.7, to: 1, name: 'Succeded', color: '#00aa00' }
          ]
        }
      }
    },
    dataLabels: { enabled: false },
    title: { text: 'Heatmap of Classification success'}
  }

  const series = data.map(toSeries)
  console.log(series)

  return (
    <div>
      <Chart options={options} series={series} width={1000} type="heatmap" height={500}/>
    </div>
  )
}

export default RegexHeatmap
