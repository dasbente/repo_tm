import { createSlice } from '@reduxjs/toolkit'
import data from '../../data/regex_data.json'

export const regexSlice = createSlice({
  name: 'regex',
  initialState: data
})

export const selectData = ({ regex }) => regex
export const regexLabels = ({ regex }) => {
  const res = new Set()
  regex.forEach(({ stats }) => stats.forEach(([ex, _]) => res.add(ex)))
  return [...res]
}

export default regexSlice.reducer
