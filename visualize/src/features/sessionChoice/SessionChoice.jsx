import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import {
  selected, select, sessionIdsSelector
} from './sessionSlice'

const SessionItem = ({ index, session }) => {
  const dispatch = useDispatch()
  const active = useSelector(selected)

  return (
    <button
      className={`list-group-item ${index === active ? 'active' : ''}`} href=""
      onClick={() => dispatch(select(index))}
    >
      Session: {session}
    </button>
  )
}

const SessionChoice = () => {
  const sessions = useSelector(sessionIdsSelector)
  return (
    <ul className="list-group">
      {sessions.map((s, i) => <SessionItem index={i} session={s} />)}
    </ul>
  )
}

export default SessionChoice
