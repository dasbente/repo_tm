import { createSlice } from '@reduxjs/toolkit'

export const sessionSlice = createSlice({
  name: 'session',
  initialState: 0,
  reducers: {
    select: (state, action) => action.payload
  }
})

export const sessionIdsSelector = ({ players }) => [...new Set(players.map(p => p.session))]
export const sessionSelector = id => ({ players }) => players.filter(p => p.session === id)
export const selected = ({ session }) => session
export const selectedSessionId = state => sessionIdsSelector(state)[selected(state)]
export const selectedSessionsData = state => sessionSelector(selectedSessionId(state))(state)

export const { select } = sessionSlice.actions

export default sessionSlice.reducer
