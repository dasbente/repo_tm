import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { curveStyleSelector, setCurveStyle } from './styleSlice'

const click = type => curveStyleSelector(type)

const CurveStyle = () => {
  const dispatch = useDispatch()
  const active = useSelector(curveStyleSelector)

  return (
    <div className="form-group">
      <label>Curve Style Selection</label><br />
      <div className="btn-group" role="group">
        {['stepline', 'straight', 'smooth'].map((style, i) => (
          <button
            className={`btn ${active == style ? 'btn-secondary' : 'btn-outline-secondary'}`}
            onClick={() => dispatch(setCurveStyle(style))} key={i}
          >
            {style}
          </button>))}
      </div>
    </div>
  )
}

export default CurveStyle
