import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { weightSelector, setWeight } from './styleSlice'

const Weight = ({ type }) => {
  const dispatch = useDispatch()
  const weight = useSelector(weightSelector(type))

  const id = type + '-weight'
  return (
    <div className="form-group">
      <label for={id}>{type}</label>
      <input
        type="number" className="form-control" id={id}
        value={weight}
        onChange={e => dispatch(setWeight({ type, weight: parseInt(e.target.value, 10) }))}
      />
    </div>
  )
}

export default Weight
