import { createSlice } from '@reduxjs/toolkit'

const styleSlice = createSlice({
  name: 'style',
  initialState: {
    curve: 'stepline',
    weight: { success: 1.0, fail: 0.0 }
  },
  reducers: {
    setWeight: (state, { type, payload }) => {
      state.weight[payload.type] = payload.weight
    },
    setCurveStyle: (state, action) => ({
      ...state, curve: action.payload
    })
  }
})

export const { setWeight, setCurveStyle } = styleSlice.actions

export const curveStyleSelector = ({ style }) => style.curve
export const weightSelector = type => ({ style }) => style.weight[type]

export default styleSlice.reducer
